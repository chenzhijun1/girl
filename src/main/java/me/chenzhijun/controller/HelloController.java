package me.chenzhijun.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by alvin on 5/20/17.
 */
@RestController
@RequestMapping(value = "/say")
public class HelloController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String sayHello() {
        return "hello";
    }

    @GetMapping(value = "/yes")
    public String sayYes(){
        return "yes";
    }


    @GetMapping(value="/test")
    public String Test(){
        return "test";
    }

    @GetMapping(value="/net/test")
    public String MyNetTest(){
        return "this is net test";
    }

}
