package me.chenzhijun.controller;

import me.chenzhijun.entity.GirlProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 设置applicaiton.yml中预设的值
 * Created by alvin on 5/20/17.
 */
@RestController
@RequestMapping("/cup")
public class ContextController {



    @Value("${cupSize}")
    private String cupSize;

    @GetMapping("/size")
    public String returnCupSize() {
        return cupSize;
    }

    @Value("${content}")
    private String content;

    @GetMapping("/content")
    public String returnContetn() {
        return content;
    }

    @Autowired
    private GirlProperties girlProperties;

@GetMapping("/girl")
    public String returnGirl(){
        return girlProperties.toString();
    }

}
