package me.chenzhijun.controller;

import me.chenzhijun.entity.Girl;
import me.chenzhijun.entity.Result;
import me.chenzhijun.enums.ResultEnum;
import me.chenzhijun.exception.GirlException;
import me.chenzhijun.impl.GirlDaoImpl;
import me.chenzhijun.utils.ResultUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;

/**
 * Created by alvin on 5/20/17.
 */

@RestController
public class GirlController {

    @Autowired
    private GirlDaoImpl girlDao;

    @PostMapping("/add")
    public Result<Girl> addGirl(@Valid Girl girl, BindingResult bindingResult) {


        if (bindingResult.hasErrors()) {
            return ResultUtils.error(ResultUtils.ERROR_CODE, bindingResult.getFieldError().getDefaultMessage());
        }

        System.out.println();
        girlDao.addGirl(girl);

        return ResultUtils.success(girl);
    }


    @PostMapping("/get")
    public void getGirl(Girl girl) throws Exception {

        ArrayList<String> list = null;

        HashMap<String,String> map = null;

        if (girl.getAge() < 10) {
            throw new GirlException(ResultEnum.PRIMARY_SCHOOL);
        }
        if (girl.getAge() > 10 && girl.getAge() < 20) {
            throw new GirlException(ResultEnum.MIDDLE_SCHOOL);
        }
        throw new GirlException(ResultEnum.UNKONW_EXCEPTION);

    }
}
