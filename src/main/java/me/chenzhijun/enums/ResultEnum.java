package me.chenzhijun.enums;

/**
 * Created by alvin on 5/21/17.
 */
public enum ResultEnum {
    UNKONW_EXCEPTION("101","未知错误"),
    PRIMARY_SCHOOL("102","还在上小学."),
    MIDDLE_SCHOOL("103","还在上中学."),
    ;

    private String code;

    private String message;

    ResultEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
