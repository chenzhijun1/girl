package me.chenzhijun.repo;

import me.chenzhijun.entity.Girl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by alvin on 5/20/17.
 */
@Repository
public interface GirlReposity extends JpaRepository<Girl, Integer> {
}
