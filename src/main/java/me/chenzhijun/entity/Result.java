package me.chenzhijun.entity;

/**
 * Created by alvin on 5/21/17.
 */
public class Result<T> {

    // error code
    private String code;

    // error msg
    private String msg;

    //具体内容
    private T data;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
