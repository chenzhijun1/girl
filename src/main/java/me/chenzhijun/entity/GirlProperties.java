package me.chenzhijun.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by alvin on 5/20/17.
 */
@Component
@ConfigurationProperties(prefix = "girl")
public class GirlProperties {
    private String name;
    private String age;
    private String cupSize;
    private String content;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCupSize() {
        return cupSize;
    }

    public void setCupSize(String cupSize) {
        this.cupSize = cupSize;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "GirlProperties{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", cupSize='" + cupSize + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
