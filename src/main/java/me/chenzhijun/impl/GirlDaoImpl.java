package me.chenzhijun.impl;

import me.chenzhijun.entity.Girl;
import me.chenzhijun.repo.GirlReposity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by alvin on 5/20/17.
 */
@Service
public class GirlDaoImpl {

    @Autowired
    private GirlReposity girlReposity;

    @Transactional
    public void addGirl(Girl girl) {
        girlReposity.save(girl);
    }

    @Transactional(value = Transactional.TxType.REQUIRED)
    private void deleteGirl(Girl girl) {
        girlReposity.delete(girl);
    }

    public Girl findGirl(){
            return new Girl();
    }
}
