package me.chenzhijun.utils;

import me.chenzhijun.entity.Result;

import java.util.Objects;

/**
 * Created by alvin on 5/21/17.
 */
public class ResultUtils {

    public static final String ERROR_CODE = "0";
    public static final String SUCCESS_CODE = "1";

    public static final String DEFAULT_SUCCESS_MESSAGE = "success";
    public static final String DEFAULT_ERROR_MESSAGE = "error";

    public static Result success(String code, String msg, Object object) {
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        result.setData(object);
        return result;
    }

    public static Result success(Object object) {
        return success(SUCCESS_CODE, DEFAULT_SUCCESS_MESSAGE, object);
    }

    public static Result success() {
        return success(SUCCESS_CODE, DEFAULT_SUCCESS_MESSAGE, null);
    }

    public static Result error(String code, String msg) {
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        result.setData(null);
        return result;
    }
}
