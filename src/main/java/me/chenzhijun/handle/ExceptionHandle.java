package me.chenzhijun.handle;

import me.chenzhijun.entity.Result;
import me.chenzhijun.exception.GirlException;
import me.chenzhijun.utils.ResultUtils;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by alvin on 5/21/17.
 */
@ControllerAdvice
public class ExceptionHandle {

    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(ExceptionHandle.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result handle(Exception e) {

        if (e instanceof GirlException) {
            logger.info("[girl异常]:{}", e);
            return ResultUtils.error(((GirlException) e).getCode(), e.getMessage());
        }

        logger.info("[系统异常]:{}", e);
        return ResultUtils.error("100", e.getMessage());

    }

}
