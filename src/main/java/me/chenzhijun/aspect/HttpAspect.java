package me.chenzhijun.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by alvin on 5/20/17.
 */
@Aspect
@Component
public class HttpAspect {

    //这三个同时存在的时候，第一个方法不执行。

//    @Before("execution(public * me.chenzhijun.controller.GirlController.*(..))")
//    public void logAll() {
//        System.out.println("拦截GirlController里面所有的方法。");
//    }
//
//    @Before("execution(public * me.chenzhijun.controller.GirlController.addGirl(..))")
//    public void logOneMethod() {
//        System.out.println("拦截，不在乎方法里面有几个参数。");
//    }
//
//    @Before("execution(public * me.chenzhijun.controller.GirlController.addGirl(Object,Object))")
//    public void logOneMethod2() {
//        System.out.println("拦截2，不在乎方法里面有几个参数。");
//    }

    private Logger logger = LoggerFactory.getLogger(HttpAspect.class);

    @Pointcut("execution(public * me.chenzhijun.controller.GirlController.addGirl(..))")
    public void log() {

    }

    @Before("log()")
    public void logBefore(JoinPoint joinPoint) {
        System.out.println("log before ...");

        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();

        HttpServletRequest request = servletRequestAttributes.getRequest();


//        request.getRequestURL();
        logger.info("url:{}", request.getRequestURL());

        logger.info("method:{}", request.getMethod());

        logger.info("ip:{}", request.getRemoteAddr());

        //类方法
        logger.info("method:{}", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.info("method:{}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());

        //参数
        logger.info("args:{}", joinPoint.getArgs());
    }

    @After("log()")
    public void logAfter() {
        System.out.println("log after ...");
        logger.info("log After ...");
    }

    @AfterReturning(pointcut = "log()", returning = "object")
    public void doAfterReturning(Object object) {
        logger.info("response:{}", object);
    }

}
