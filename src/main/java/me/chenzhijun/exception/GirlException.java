package me.chenzhijun.exception;

import me.chenzhijun.enums.ResultEnum;

/**
 * Created by alvin on 5/21/17.
 */
public class GirlException extends RuntimeException {

    private String code;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public GirlException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }
}
