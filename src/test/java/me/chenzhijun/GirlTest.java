package me.chenzhijun;

import me.chenzhijun.entity.Girl;
import me.chenzhijun.impl.GirlDaoImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by alvin on 5/21/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GirlTest {

    @Autowired
    private GirlDaoImpl girlDao;


    @Test
    public void findTest() {
        Girl girl = girlDao.findGirl();

        System.out.println(girl);
    }
}
